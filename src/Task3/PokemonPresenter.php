<?php

namespace BinaryStudioAcademy\Task3;

class PokemonPresenter
{
    private $pokemons;

    public function __construct(array $pokemons)
    {
        $this->pokemons = $pokemons;
    }

    /**
     * Returns html-list (ul-li) of images (img)
     *
     * @return string
     */
    public function present(): string
    {
        $return="<ul>";
        foreach ($this->pokemons as $value){
             $return=$return.'<li><img src="https://img.pokemondb.net/artwork/'.$value.'.jpg"></li>';
        }
        return $return.'</ul>';

    }
}
