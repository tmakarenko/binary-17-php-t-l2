<?php
/**
 * Created by PhpStorm.
 * User: endzait
 * Date: 24.05.17
 * Time: 18:17
 */

namespace BinaryStudioAcademy\Task1;


use BinaryStudioAcademy\Task1\Pokemon;

class Bulbasaur implements Pokemon
{
    public function battleCry(): string
    {
        return "Bool bool!";
    }

    public function imageUrl(): string
    {
        return "https://img.pokemondb.net/artwork/bulbasaur.jpg";
    }
}
