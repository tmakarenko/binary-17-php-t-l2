<?php
/**
 * Created by PhpStorm.
 * User: endzait
 * Date: 24.05.17
 * Time: 18:17
 */

namespace BinaryStudioAcademy\Task1;


use BinaryStudioAcademy\Task1\Pokemon;

class Pickachu implements Pokemon
{
    public function battleCry(): string
    {
        return "Pika-Pika!";
    }

    public function imageUrl(): string
    {
        return "https://img.pokemondb.net/artwork/pikachu.jpg";
    }
}
