<?php

namespace BinaryStudioAcademy\Task1;

class PokemonTrainer
{
    public function pick(Pokemon $pokemon): string
    {

        return substr(get_class($pokemon),26).": ".$pokemon->battleCry();
    }
}
